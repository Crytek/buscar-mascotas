import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Login from './pages/auth/Login';
import Register from './pages/auth/Register';

import Publications from './pages/Publications';
import PublicationCreate from './pages/PublicationCreate';
import PublicationDetail from './pages/PublicationDetail';

import ProfilePage from './pages/Profile';

import './styles/app.scss';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/auth/login" element={<Login />} />
        <Route path="/auth/register" element={<Register />} />

        <Route path="/" element={<Publications />} />
        <Route path="/publicacion/detalle" element={<PublicationDetail />} />
        <Route path="/publicacion/crear" element={<PublicationCreate />} />

        <Route path="/perfil" element={<ProfilePage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
