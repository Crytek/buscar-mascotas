import React from 'react';
import { Link } from 'react-router-dom';

import Slider from 'react-slick';

import imgLogo from '../images/logo.png';
import imgProfile from '../images/profile.png';

import imgBanner1 from '../images/banner-home/1.png';
import imgBanner2 from '../images/banner-home/2.png';
import imgBanner3 from '../images/banner-home/3.png';

import imgCategory1 from '../images/category-1.png';
import imgCategory2 from '../images/category-2.png';
import imgCategory3 from '../images/category-3.png';
import imgCategory4 from '../images/category-4.png';

import imgSearch from '../images/search.png';
import imgAdd from '../images/add.png';

import imgImage1 from '../images/image-1.png';

const Publications = () => {
  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 2000,
    cssEase: 'linear',
  };

  return (
    <main id="page-publications">
      <header>
        <Link to="/">
          <img src={imgLogo} alt="FindPet" width={50} />
        </Link>

        <h1>BUSCA MASCOTAS</h1>

        <Link to="/perfil" className="btn-profile">
          <img src={imgProfile} alt="FindPet" width={45} />
        </Link>
      </header>

      <section id="banner">
        <Slider {...settings}>
          <div className="image">
            <img src={imgBanner1} alt="Banner1" />
          </div>
          <div className="image">
            <img src={imgBanner2} alt="Banner2" />
          </div>
          <div className="image">
            <img src={imgBanner3} alt="Banner3" />
          </div>
          <div className="image">
            <img src={imgBanner1} alt="Banner1" />
          </div>
          <div className="image">
            <img src={imgBanner2} alt="Banner2" />
          </div>
        </Slider>
      </section>

      <section id="publications-content" className="container">
        <section id="categories">
          <a href="#!" className="category">
            <img src={imgCategory1} alt="Encontrar mascota" />
            <p>Buscar</p>
          </a>

          <a href="#!" className="category">
            <img src={imgCategory2} alt="Buscar mascota" />
            <p>Encontrar</p>
          </a>

          <a href="#!" className="category">
            <img src={imgCategory3} alt="Notas sobre mascota" />
            <p>Buscar</p>
          </a>

          <a href="#!" className="category">
            <img src={imgCategory4} alt="Servicios para mascota" />
            <p>Servicios</p>
          </a>
        </section>

        <section id="search">
          <form action="">
            <input
              className="form-control"
              type="search"
              placeholder="Buscar..."
            />
            <button>
              <img src={imgSearch} alt="Buscar ..." />
            </button>
          </form>
        </section>

        <section id="publications-list">
          <div className="top">
            <h2>Publicaciones</h2>

            <Link to="/publicacion/crear">
              <img src={imgAdd} alt="Agregar" width={30} />
            </Link>
          </div>

          <div className="items">
            {[...Array(6)].map((item, index) => (
              <div className="item" key={index}>
                <div className="picture">
                  <img src={imgImage1} alt="Image" />
                </div>

                <div className="information">
                  <h3 className="title">Lorem Ipsum {index + 1}</h3>
                  <p className="description">
                    Lorem Ipsum es simplemente el texto de relleno de las
                    imprentas y archivos de texto. Lorem Ipsum ha sido el texto
                    de relleno estándar de las industrias desde el año 1500
                  </p>
                  <p className="date">31/01/2022</p>
                </div>
              </div>
            ))}
          </div>
        </section>
      </section>
    </main>
  );
};

export default Publications;
